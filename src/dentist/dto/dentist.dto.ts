export class DentistDto {
    _id?: string;
    name: string;
    lastName: string;
    email: string;
    phone: string;
    potho?: string;
  }
  