import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import { DentistService } from './dentist.service';
import { DentistDto } from './dto/dentist.dto';

@Controller('api/dentists')
export class DentistController {
  constructor(private dentistService: DentistService) {}

  @Get()
  async findAll(@Res() res: Response) {
    const dentists = await this.dentistService.findAll();
    return res.status(HttpStatus.OK).json({ dentists });
  }

  @Post()
  async create(@Res() res: Response, @Body() dentist: DentistDto) {
    const newDentist = await this.dentistService.create(dentist);
    return res.status(HttpStatus.CREATED).json({ user: newDentist });
  }
}
