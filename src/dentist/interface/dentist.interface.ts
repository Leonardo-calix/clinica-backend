import { Document } from "mongoose";

export interface Dentist extends Document {
    _id?: string;
    name: string;
    lastName: string;
    email: string;
    phone: string;
    potho: string;
}
