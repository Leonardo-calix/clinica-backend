import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DentistDto } from './dto/dentist.dto';
import { Dentist } from './interface/dentist.interface';

@Injectable()
export class DentistService {
  constructor(@InjectModel('Dentist') private dentistModel: Model<Dentist>) {}

  async create(dentist: DentistDto): Promise<Dentist> {
    const newDentist = new this.dentistModel(dentist);
    await newDentist.save();
    return newDentist;
  }

  async findAll(): Promise<Dentist[]> {
    const dentists = await this.dentistModel.find();
    return dentists;
  }

  async findById(id: string): Promise<Dentist> {
    const dentist = await this.dentistModel.findById(id);
    return dentist;
  }

  async update(id: string, dentist: DentistDto): Promise<Dentist> {
    const dentistUpdated = await this.dentistModel.findByIdAndUpdate(
      id,
      dentist,
      {
        new: true,
      },
    );
    return dentistUpdated;
  }

  async delete(id: string): Promise<Dentist> {
    const dentist = await this.dentistModel.findByIdAndRemove(id);
    return dentist;
  }
}
