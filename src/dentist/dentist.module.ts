import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DentistController } from './dentist.controller';
import { DentistService } from './dentist.service';
import { dentistSchema } from './schemas/dentist.schemas';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Dentist', schema: dentistSchema }])],
  controllers: [DentistController],
  providers: [DentistService]
})
export class DentistModule {}
