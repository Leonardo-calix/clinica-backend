import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './users/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { DentistModule } from './dentist/dentist.module';

@Module({
  imports: [
    UserModule,
    MongooseModule.forRoot(
      'mongodb+srv://calix:calix1994@clinica.ucqmg.mongodb.net/clinica-db?retryWrites=true&w=majority',
    ),
    DentistModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
