export class UserDto {
  _id?: string;
  name: string;
  lastName: string;
  email: string;
  password: string;
}
