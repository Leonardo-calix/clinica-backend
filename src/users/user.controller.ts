import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { UserDto } from './dto/user.dto';
import { UserService } from './user.service';

@Controller('api/users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  async findAll(@Res() res: Response) {
    const users = await this.userService.findAll();
    return res.status(HttpStatus.OK).json({ users });
  }

  @Post()
  async create(@Res() res: Response, @Body() user: UserDto) {
    const newUser = await this.userService.create(user);
    return res.status(HttpStatus.CREATED).json({ user: newUser });
  }

  @Put()
  async update(
    @Res() res: Response,
    @Body() user: UserDto,
    @Query('id') id: string,
  ) {
    const userUpdated = await this.userService.update(id, user);
    if(!userUpdated) throw new NotFoundException('User no existe');
    return res.status(HttpStatus.OK).json({ userUpdated });
  }

  @Delete()
  async remove(@Res() res: Response, @Query('id') id: string) {
    const userRemoved = await this.userService.delete(id);
    if(!userRemoved) throw new NotFoundException('User no existe');
    return res.status(HttpStatus.OK).json({ userRemoved });
  }
}
