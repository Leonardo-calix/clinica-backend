import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private userModel: Model<User>) {}

  async create(user: UserDto): Promise<User> {
    const newUser = new this.userModel(user);
    await newUser.save();
    return newUser;
  }

  async findAll(): Promise<User[]> {
    const users = await this.userModel.find();
    return users;
  }

  async findById(id: string): Promise<User> {
    const user = await this.userModel.findById(id);
    return user;
  }

  async update(id: string, user: UserDto): Promise<User> {
    const userUpdated = await this.userModel.findByIdAndUpdate(id, user, {
      new: true,
    });
    return userUpdated;
  }

  async delete(id: string): Promise<User> {
    const user = await this.userModel.findByIdAndRemove(id);
    return user;
  }
}
